class EntriesController < ApplicationController
	before_action :set_entry, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!

	respond_to :html

	def index
		@entries = current_user.entries.paginate(:page => params[:page], :per_page => 25)
		respond_with(@entries)
	end

	def show
		respond_with(@entry)
	end

	def new
		@entry = Entry.new
		respond_with(@entry)
	end

	def edit
	end

	def create
		@entry = current_user.entries.create(entry_params)
		if @entry.save
			flash[:notice] = "Entry saved!"
		else
			flash[:alert] = "Entry not saved!"
		end
		respond_with(@entry)
	end

	def update
		@entry.update(entry_params)
		respond_with(@entry)
	end

	def destroy
		@entry.destroy
		respond_with(@entry)
	end

	private
		def set_entry
			@entry = current_user.entries.find_by(id: params[:id])
		end

		def entry_params
			params.require(:entry).permit(:title, :content)
		end
end
