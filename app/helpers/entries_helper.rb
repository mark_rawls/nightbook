module EntriesHelper
	def formatDate(date)
		"#{date.month}/#{date.day}/#{date.year}"
	end

	def html_format(entry)
		entry.gsub('<', '&lt').gsub('>', '&gt').gsub("\n", '<br />').html_safe
	end
end
